package tw.com.shoesconn.iot.webAPI;

public class ApiMachieInfo {

    /**
     * success : true
     * mac : 98-F4-AB-DA-2D-75
     * model : Aadc-55-A
     * serial : 55-A-001
     * triming : 1
     * sewing : 3
     * remark : Zone A
     * line_id : 1
     * line_name : A1
     * theoretical_output : 87
     * zone_id : 1
     * zone_name : Zone A
     * work_order : NHK-19110298
     * sku : H019-127
     * target_output : 344
     * message: " "
     */

    private boolean success;
    private String mac;
    private String model;
    private String serial;
    private int triming;
    private int sewing;
    private String remark;
    private int line_id;
    private String line_name;
    private int theoretical_output;
    private int zone_id;
    private String zone_name;
    private String message;
    private String work_order;
    private String sku;
    private int target_output;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public int getTriming() {
        return triming;
    }

    public void setTriming(int triming) {
        this.triming = triming;
    }

    public int getSewing() {
        return sewing;
    }

    public void setSewing(int sewing) {
        this.sewing = sewing;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getLine_id() {
        return line_id;
    }

    public void setLine_id(int line_id) {
        this.line_id = line_id;
    }

    public String getLine_name() {
        return line_name;
    }

    public void setLine_name(String line_name) {
        this.line_name = line_name;
    }

    public int getTheoretical_output() {
        return theoretical_output;
    }

    public void setTheoretical_output(int theoretical_output) {
        this.theoretical_output = theoretical_output;
    }

    public int getZone_id() {
        return zone_id;
    }

    public void setZone_id(int zone_id) {
        this.zone_id = zone_id;
    }

    public String getZone_name() {
        return zone_name;
    }

    public void setZone_name(String zone_name) {
        this.zone_name = zone_name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getWork_order() {
        return work_order;
    }

    public void setWork_order(String work_order) {
        this.work_order = work_order;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public int getTarget_output() {
        return target_output;
    }

    public void setTarget_output(int target_output) {
        this.target_output = target_output;
    }
}
