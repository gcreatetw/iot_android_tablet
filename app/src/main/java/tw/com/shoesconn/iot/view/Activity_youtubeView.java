package tw.com.shoesconn.iot.view;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import tw.com.shoesconn.iot.R;
import tw.com.shoesconn.iot.global;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

public class Activity_youtubeView extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    private YouTubePlayerView mYouTubePlayerView;
    public static final String API_KEY = "youtube";
    public static final String VIDEO_ID = global.YoutubeVideoID;
    private Button mBack;
    private TextView lessonName;
    private TextView videoDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube_view);

        mYouTubePlayerView = findViewById(R.id.youtube_view);
        mYouTubePlayerView.initialize(API_KEY, this);

        lessonName = findViewById(R.id.youtube_lessonName);
        lessonName.setText(getIntent().getStringExtra("lessonName"));

        mBack = findViewById(R.id.youtube_view_back);
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        videoDate = findViewById(R.id.youtube_videoDate);
        videoDate.setText("Date : " + this.getIntent().getStringExtra("videoDate").substring(0,10));
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        Toast.makeText(this, "onInitializationSuccess!", Toast.LENGTH_SHORT).show();
        if (youTubePlayer == null) {
            return;
        }

        if (!b) {
            youTubePlayer.cueVideo(VIDEO_ID);
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this, "Failed to initialize.", Toast.LENGTH_LONG).show();
    }
}
