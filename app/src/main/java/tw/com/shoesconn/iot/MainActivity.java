package tw.com.shoesconn.iot;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import tw.com.shoesconn.iot.view.Fragment_equipment_info;
import tw.com.shoesconn.iot.view.Fragment_historical_data;
import tw.com.shoesconn.iot.view.Fragment_main;
import tw.com.shoesconn.iot.view.Fragment_worksheet;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView bottomNavigation;
    private Fragment_main f_main = new Fragment_main();
    private Fragment_worksheet f_worksheet = new Fragment_worksheet();
    private Fragment_historical_data f_historical_date = new Fragment_historical_data();
    private Fragment_equipment_info f_equipment_info = new Fragment_equipment_info();
    private InnerRecevier innerRecevier;
    private boolean isReceiverRegistered = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(0x80000000, 0x80000000);
        setContentView(R.layout.activity_main);
        global.initWebAPI();


        // get WindowSize
        new Generalfunction().getWindowSize(this);

        bottomNavigation = findViewById(R.id.bottomNavigation);
        bottomNavigation.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);
        bottomNavigation.setItemIconTintList(null);
        new Generalfunction().replaceFragment(MainActivity.this, f_main);

        //-----------------監聽手機功能鍵----------------
        innerRecevier = new InnerRecevier();
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        registerReceiver(innerRecevier, intentFilter);
        isReceiverRegistered = true;

    }

    private BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

            switch (menuItem.getItemId()) {
                case R.id.navigation_main:
                    new Generalfunction().replaceFragment(MainActivity.this, f_main);
                    break;

                case R.id.navigation_worksheet:
                    new Generalfunction().replaceFragment(MainActivity.this, f_worksheet);
                    break;

                case R.id.navigation_historical_date:
                    new Generalfunction().replaceFragment(MainActivity.this, f_historical_date);
                    break;

                case R.id.navigation_equipment_info:
                    new Generalfunction().replaceFragment(MainActivity.this, f_equipment_info);
                    break;

                default:
                    return false;
            }
            return true;
        }
    };

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 1) {
            fragmentManager.popBackStackImmediate();

            if (fragmentManager.getFragments().get(0).toString().substring(0, 13).equals("Fragment_main")) {
                bottomNavigation.getMenu().findItem(R.id.navigation_main).setChecked(true);
            } else if (fragmentManager.getFragments().get(0).toString().substring(0, 18).equals("Fragment_worksheet")) {
                bottomNavigation.getMenu().findItem(R.id.navigation_worksheet).setChecked(true);
            } else if (fragmentManager.getFragments().get(0).toString().substring(0, 24).equals("Fragment_historical_date")) {
                bottomNavigation.getMenu().findItem(R.id.navigation_historical_date).setChecked(true);
            } else if (fragmentManager.getFragments().get(0).toString().substring(0, 23).equals("Fragment_equipment_info")) {
                bottomNavigation.getMenu().findItem(R.id.navigation_equipment_info).setChecked(true);
            }

        } else {
            finish();
        }
    }


    // ----------------------------複寫監聽手機功能鍵 function----------------------------------------------------------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return true;
        }
        if (KeyEvent.KEYCODE_HOME == keyCode) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isReceiverRegistered) {
            unregisterReceiver(innerRecevier);
            isReceiverRegistered = false;// set it back to false.
        }
    }

}
