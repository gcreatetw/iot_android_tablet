package tw.com.shoesconn.iot.webAPI;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;


public interface Webapi {


    String PATH = "demoweb.gcreate.com.tw/tailorIOT/public";
    String SERVER_IP = "https://" + PATH + "/api/";


    //------------------     GET ----------------------------
    @Headers({"Content-Type: application/json"})
    @GET("app/getTime")
    Call<ApiCurrentTime> GetCurrentTime();

    @Headers({"Content-Type: application/json"})
    @GET("app/getMachineInfo/{macID}")
    Call<ApiMachieInfo> GetMachineInfo(@Path("macID") String macID);

    @Headers({"Content-Type: application/json"})
    @GET("app/getMachineStatus/{macID}")
    Call<ApiMachineStatus> GetMachineStatus(@Path("macID") String macID);

    @Headers({"Content-Type: application/json"})
    @GET("app/getWorkSheet/{model}")
    Call<ApiWorkSheet> GetWorkSheetData(@Path("model") String model);

    @Headers({"Content-Type: application/json"})
    @GET("app/getMachineHistoryStatus/{macID}")
    Call<ApiMachineHistoryStatus> getMachineHistoryStatus(@Path("macID") String macID);


    //--------------- POST  ---------------------------
    @Headers({"Content-Type: application/json"})
    @POST("app/login")
    Call<ApiLogin> GetLoginResult(@Body JsonObject jsonObject);

    @Headers({"Content-Type: application/json"})
    @POST("app/testupload")
    Call<ApiUploadtest> GetUploadTestResult(@Body JsonObject jsonObject);

    @Headers({"Content-Type: application/json"})
    @POST("app/searchWorkSheet")
    Call<ApiSearchWorkSheet> GetSearchWorkSheetResult(@Body JsonObject jsonObject);

    @Headers({"Content-Type: application/json"})
    @POST("setCountList")
    Call<ApiSetCountList> GetCountListResult(@Body JsonObject jsonObject);

}
