package tw.com.shoesconn.iot.view;

import android.media.MediaPlayer;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;

import tw.com.shoesconn.iot.R;
import tw.com.shoesconn.iot.global;


public class Fragment_equipment_info extends Fragment {

    private View view;
    private TextView tv_account_name, tv_account_workerID, tv_acount_occupation;
    private TextView tv_eq_zone, tv_eq_line, tv_eq_no, tv_eq_model, tv_eq_mac;
    private SwitchCompat alertSwitch, notifySwitch;
    private MediaPlayer player;

    public Fragment_equipment_info() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_equipment_info, container, false);
        tv_account_name = view.findViewById(R.id.tv_account_name);
        tv_account_workerID = view.findViewById(R.id.tv_account_workerID);
        tv_acount_occupation = view.findViewById(R.id.tv_account_occupation);
        tv_eq_zone = view.findViewById(R.id.tv_equipInfo_zone);
        tv_eq_line = view.findViewById(R.id.tv_equipInfo_line);
        tv_eq_no = view.findViewById(R.id.tv_equipInfo_mcNo);
        tv_eq_model = view.findViewById(R.id.tv_equipInfo_mcModel);
        tv_eq_mac = view.findViewById(R.id.tv_equipInfo_mac);

        //---------   Account ------------
        tv_account_name.setText(global.objectApiLogin.getName() + "");
        tv_account_workerID.setText(global.objectApiLogin.getWorker_id());

        String occupation[] = {"administrator", "manager", "supervisor", "employee", "operator"};
        tv_acount_occupation.setText(occupation[Integer.parseInt(global.objectApiLogin.getOccupation())]);

        //---------   Equipment info ------------
        tv_eq_zone.setText(global.objectMachineInfo.getZone_name());
        tv_eq_line.setText(global.objectMachineInfo.getLine_name());
        tv_eq_no.setText(global.objectMachineInfo.getSerial());
        tv_eq_model.setText(global.objectMachineInfo.getModel());
        tv_eq_mac.setText(global.objectMachineInfo.getMac());

        //---------   Switch ------------
        alertSwitch = view.findViewById(R.id.switch_alert);
        notifySwitch = view.findViewById(R.id.switch_notification);


        alertSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    tv_eq_zone.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            play();
                        }
                    });
                }else{
                    tv_eq_zone.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            stopPlayer();
                        }
                    });
                }
            }
        });

        return view;
    }

    //---------------------------------- Media  player function ------------------------------------
    private void play() {
        if (player == null) {
            player = MediaPlayer.create(getActivity(), R.raw.song);
            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    stopPlayer();
                }
            });
        }
        player.start();
    }

    private void stopPlayer() {
        if (player != null ) {
            player.stop();
            player.reset();
            player.release();
            player = null;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        stopPlayer();
    }
}
