package tw.com.shoesconn.iot.recyclerModel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.math.BigDecimal;
import java.util.List;

import tw.com.shoesconn.iot.R;
import tw.com.shoesconn.iot.global;
import tw.com.shoesconn.iot.webAPI.ApiMachineHistoryStatus;


public class Adapter_historicalData extends RecyclerView.Adapter<Adapter_historicalData.ViewHolder> {

    private Context mContext;
    private List<ApiMachineHistoryStatus.HistoryBean> list;

    public Adapter_historicalData(Context mContext, List<ApiMachineHistoryStatus.HistoryBean> list) {
        this.mContext = mContext;
        this.list = list;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_historical, parent, false);
        Adapter_historicalData.ViewHolder vh = new Adapter_historicalData.ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.card_tv_history_date.setText(list.get(position).getData_date());
        holder.card_tv_history_line.setText(global.objectMachineInfo.getLine_name());
        holder.card_tv_history_workOder.setText(list.get(position).getWorker_order());
        if (list.get(position).getConversion_rate() == 0.0) {
            holder.card_tv_history_conversion_rate.setText((int) list.get(position).getConversion_rate() + " %");
        } else {
            BigDecimal b = new BigDecimal(list.get(position).getConversion_rate());
            float f1 = b.setScale(2, BigDecimal.ROUND_HALF_UP).floatValue();
            holder.card_tv_history_conversion_rate.setText((f1 + " %"));
        }
        holder.card_tv_history_defective.setText(list.get(position).getTotal_defective());
        holder.card_tv_history_actual_output.setText(list.get(position).getTotal_actual_output());
        holder.card_tv_history_target_output.setText(list.get(position).getTotal_targetl_output() + "");
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        //      init card
        private TextView card_tv_history_date, card_tv_history_line, card_tv_history_workOder, card_tv_history_conversion_rate, card_tv_history_defective, card_tv_history_actual_output, card_tv_history_target_output;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            this.card_tv_history_date = itemView.findViewById(R.id.card_tv_history_date);
            this.card_tv_history_line = itemView.findViewById(R.id.card_tv_history_line);
            this.card_tv_history_workOder = itemView.findViewById(R.id.card_tv_history_workOder);
            this.card_tv_history_conversion_rate = itemView.findViewById(R.id.card_tv_history_conversion_rate);
            this.card_tv_history_defective = itemView.findViewById(R.id.card_tv_history_defective);
            this.card_tv_history_actual_output = itemView.findViewById(R.id.card_tv_history_actual_output);
            this.card_tv_history_target_output = itemView.findViewById(R.id.card_tv_history_target_output);


        }
    }
}
