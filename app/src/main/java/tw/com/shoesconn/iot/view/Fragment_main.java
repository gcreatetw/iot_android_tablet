package tw.com.shoesconn.iot.view;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import tw.com.shoesconn.iot.DonutProgress;
import tw.com.shoesconn.iot.Generalfunction;
import tw.com.shoesconn.iot.R;
import tw.com.shoesconn.iot.global;
import tw.com.shoesconn.iot.webAPI.ApiMachieInfo;
import tw.com.shoesconn.iot.webAPI.ApiMachineStatus;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Fragment_main extends Fragment {

    private FragmentActivity fragmentActivity;
    private DonutProgress mdonutProgress;
    private TextView tv_actualOutput, tv_targetOutput, tv_defective, tv_current_date, tv_current_time;
    private TextView tv_main_zone, tv_main_line, tv_main_workOrder, tv_main_style;
    private Handler handler = new Handler();
    private Runnable time, production;
    private int value_target, value_actual;

    private ApiMachineStatus objectApiMachineStatus;
    private static Boolean firsttime = true;

    public Fragment_main() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_main, container, false);
        mdonutProgress = view.findViewById(R.id.progress_circular);
        fragmentActivity = getActivity();
        //--- Time ---
        tv_current_date = view.findViewById(R.id.tv_current_date);
        tv_current_time = view.findViewById(R.id.tv_current_time);
        //--- 生產資訊---
        tv_actualOutput = view.findViewById(R.id.tv_main_actualOutput);
        tv_targetOutput = view.findViewById(R.id.tv_main_targetOutput);
        tv_defective = view.findViewById(R.id.tv_main_defective);
        //--- 機台資訊---
        tv_main_zone = view.findViewById(R.id.tv_main_zone);
        tv_main_line = view.findViewById(R.id.tv_main_line);
        tv_main_workOrder = view.findViewById(R.id.tv_main_workOrder);
        tv_main_style = view.findViewById(R.id.tv_main_style);
        //--- 測試模式 ---
        Button btn_textmode = view.findViewById(R.id.btn_textmode);
        btn_textmode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Generalfunction().replaceFragment(getActivity(), new Fragment_main_textmode());
            }
        });

        routineUpdateTime();
        routineUpdateProductionStatus();
        getMachineInfo();

        return view;
    }


    //------------------------ get machine info -----------------
    private void getMachineInfo() {
        Call<ApiMachieInfo> call = global.webapi.GetMachineInfo(global.macID);
        call.enqueue(new Callback<ApiMachieInfo>() {
            @Override
            public void onResponse(@NonNull Call<ApiMachieInfo> call, @NonNull Response<ApiMachieInfo> response) {
                global.objectMachineInfo = response.body();
                assert global.objectMachineInfo != null;
                if (global.objectMachineInfo.isSuccess()) {
                    tv_main_zone.setText(global.objectMachineInfo.getZone_name());
                    tv_main_line.setText(String.format("%s%s", getResources().getString(R.string.string_main_line), global.objectMachineInfo.getLine_name()));
                    tv_main_workOrder.setText(String.format("%s%s", getResources().getString(R.string.string_main_work_order), global.objectMachineInfo.getWork_order()));
                    tv_main_style.setText(String.format("%s%s", getResources().getString(R.string.string_main_style_sku), global.objectMachineInfo.getSku()));
                    tv_targetOutput.setText(String.valueOf(global.objectMachineInfo.getTarget_output()));
                    value_target = global.objectMachineInfo.getTarget_output();
                } else {
                    Toast.makeText(getActivity(), global.objectMachineInfo.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiMachieInfo> call, @NonNull Throwable t) {
                Toast.makeText(getActivity(), "網路連線異常.請檢察網路連線", Toast.LENGTH_LONG).show();
            }
        });
    }

    // -------------------------- Update current time -----------------------------------
    private void routineUpdateTime() {
        time = new Runnable() {
            public void run() {
                handler.postDelayed(this, 1000);
                SimpleDateFormat formatter_1 = new SimpleDateFormat("yyyy / MM / dd", Locale.ENGLISH);
                SimpleDateFormat formatter_2 = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
                Date curDate = new Date(System.currentTimeMillis());
                tv_current_date.setText(formatter_1.format(curDate));
                tv_current_time.setText(formatter_2.format(curDate));
            }
        };
        handler.post(time);
    }

    // -------------------------- Update production  info -----------------------------------
    private void routineUpdateProductionStatus() {
        production = new Runnable() {
            public void run() {
                handler.postDelayed(this, 10000);
                Call<ApiMachineStatus> call = global.webapi.GetMachineStatus(global.macID);
                call.enqueue(new Callback<ApiMachineStatus>() {
                    @Override
                    public void onResponse(@NonNull Call<ApiMachineStatus> call, @NonNull Response<ApiMachineStatus> response) {

                        objectApiMachineStatus = response.body();
                        assert objectApiMachineStatus != null;
                        if (objectApiMachineStatus.getTotal_actual_output() != null) {
                            tv_actualOutput.setText(objectApiMachineStatus.getTotal_actual_output());
                            value_actual = Integer.parseInt(objectApiMachineStatus.getTotal_actual_output());

                            if (value_target != 0) {
                                if ((value_actual * 100) / value_target <= 40)
                                    mdonutProgress.setFinishedStrokeColor(ContextCompat.getColor(fragmentActivity, R.color.percent0to40));
                                else if ((value_actual * 100) / value_target > 40 && (value_actual * 100) / value_target <= 80)
                                    mdonutProgress.setFinishedStrokeColor(ContextCompat.getColor(fragmentActivity, R.color.percent40to80));
                                else if ((value_actual * 100) / value_target > 80) {
                                    mdonutProgress.setFinishedStrokeColor(ContextCompat.getColor(fragmentActivity, R.color.percent80to100));
                                }
                                mdonutProgress.setProgress((value_actual * 100) / value_target);
                            }
                            if (firsttime) {
                                global.value_1st_sewing = objectApiMachineStatus.getLast_sewing();
                                global.value_1st_triming = objectApiMachineStatus.getLast_triming();
                                firsttime = false;
                            }
                        }
                        tv_defective.setText(objectApiMachineStatus.getTotal_defective());
                    }

                    @Override
                    public void onFailure(@NonNull Call<ApiMachineStatus> call,@NonNull  Throwable t) {
                        Toast.makeText(getActivity(), "網路連線異常.請檢察網路連線", Toast.LENGTH_LONG).show();
                    }
                });
            }
        };
        handler.post(production);
    }

    @Override
    public void onStop() {
        super.onStop();
        handler.removeCallbacks(production);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(production);
        handler.removeCallbacks(time);
    }

}
