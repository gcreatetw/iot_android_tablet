package tw.com.shoesconn.iot.webAPI;

import java.util.List;

public class ApiMachineHistoryStatus {


    /**
     * success : true
     * history : [{"total_defective":"0","total_actual_output":"0","total_worktime":"0","data_date":"2020-05-27","total_targetl_output":344,"worker_order":"test","conversion_rate":0},{"total_defective":"0","total_actual_output":"0","total_worktime":"0","data_date":"2020-05-28","total_targetl_output":344,"worker_order":"test","conversion_rate":0},{"total_defective":"0","total_actual_output":"0","total_worktime":"0","data_date":"2020-05-29","total_targetl_output":344,"worker_order":"test","conversion_rate":0},{"total_defective":"0","total_actual_output":"0","total_worktime":"0","data_date":"2020-05-30","total_targetl_output":344,"worker_order":"test","conversion_rate":0},{"total_defective":"0","total_actual_output":"0","total_worktime":"0","data_date":"2020-05-31","total_targetl_output":344,"worker_order":"test","conversion_rate":0},{"total_defective":"0","total_actual_output":"0","total_worktime":"0","data_date":"2020-06-01","total_targetl_output":344,"worker_order":"test","conversion_rate":0},{"total_defective":"0","total_actual_output":"0","total_worktime":"0","data_date":"2020-06-02","total_targetl_output":344,"worker_order":"test","conversion_rate":0},{"total_defective":"0","total_actual_output":"0","total_worktime":"0","data_date":"2020-06-03","total_targetl_output":344,"worker_order":"test","conversion_rate":0}]
     */

    private boolean success;
    private List<HistoryBean> history;
    private String message;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<HistoryBean> getHistory() {
        return history;
    }

    public void setHistory(List<HistoryBean> history) {
        this.history = history;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class HistoryBean {
        /**
         * total_defective : 0
         * total_actual_output : 0
         * total_worktime : 0
         * data_date : 2020-05-27
         * total_targetl_output : 344
         * worker_order : test
         * conversion_rate : 0
         */

        private String total_defective;
        private String total_actual_output;
        private String total_worktime;
        private String data_date;
        private int total_targetl_output;
        private String worker_order;
        private float conversion_rate;

        public String getTotal_defective() {
            return total_defective;
        }

        public void setTotal_defective(String total_defective) {
            this.total_defective = total_defective;
        }

        public String getTotal_actual_output() {
            return total_actual_output;
        }

        public void setTotal_actual_output(String total_actual_output) {
            this.total_actual_output = total_actual_output;
        }

        public String getTotal_worktime() {
            return total_worktime;
        }

        public void setTotal_worktime(String total_worktime) {
            this.total_worktime = total_worktime;
        }

        public String getData_date() {
            return data_date;
        }

        public void setData_date(String data_date) {
            this.data_date = data_date;
        }

        public int getTotal_targetl_output() {
            return total_targetl_output;
        }

        public void setTotal_targetl_output(int total_targetl_output) {
            this.total_targetl_output = total_targetl_output;
        }

        public String getWorker_order() {
            return worker_order;
        }

        public void setWorker_order(String worker_order) {
            this.worker_order = worker_order;
        }

        public float getConversion_rate() {
            return conversion_rate;
        }

        public void setConversion_rate(int conversion_rate) {
            this.conversion_rate = conversion_rate;
        }
    }
}
