package tw.com.shoesconn.iot.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import tw.com.shoesconn.iot.Generalfunction;
import tw.com.shoesconn.iot.R;
import tw.com.shoesconn.iot.global;
import tw.com.shoesconn.iot.webAPI.ApiMachineStatus;
import tw.com.shoesconn.iot.webAPI.ApiUploadtest;
import com.google.gson.JsonObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Fragment_main_textmode extends Fragment implements TextView.OnEditorActionListener {

    private View view;
    private DecimalFormat decimalFormat;
    private ApiMachineStatus objectMachineStatus;
    private ApiUploadtest objectApiUploadtest;
    private static Button btn_start_pause, btn_resetDialog, btn_upload;
    private static TextView tvTimer, tv_length, tv_sewing, tv_triming;
    private static EditText meditSpacing;

    private long startTime = 0L, timeInMilliseconds = 0L, updateTime = 0L;
    private static long timeSwapBuff = 0L;
    private static int value_sewing;
    private static int value_2nd_sewing, value_2nd_triming;

    private String min, sec;

    private Handler TimerHandler = new Handler();
    private Handler getDataHandler = new Handler();
    private Runnable sewingTriming, updateTimerThread;

    private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    private SimpleDateFormat formatter2 = new SimpleDateFormat("HH:mm:ssXXX", Locale.ENGLISH);
    private Date currentTime = new Date(System.currentTimeMillis());

    public Fragment_main_textmode() {
    }

    @Override
    public void onStart() {
        super.onStart();
        tvTimer.setText(getResources().getString(R.string.string_testMode_00_00_00));
        timeSwapBuff = 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_main_textmode, container, false);
        LinearLayout mLinearLayout = view.findViewById(R.id.ly_testMode_back);
        btn_start_pause = view.findViewById(R.id.btn_start_pause);
        btn_resetDialog = view.findViewById(R.id.btn_reset);
        btn_upload = view.findViewById(R.id.btn_upload2);
        tvTimer = view.findViewById(R.id.tvTimer);
        meditSpacing = view.findViewById(R.id.ed_spacing);
        tv_length = view.findViewById(R.id.tv_length);
        tv_sewing = view.findViewById(R.id.tv_sewing);
        tv_triming = view.findViewById(R.id.tv_triming);

        value_sewing = Integer.parseInt((String) tv_sewing.getText());

        // -----------   Edit Text -------------
        meditSpacing.setOnEditorActionListener(this);
        meditSpacing.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    meditSpacing.setText("");
                }
            }
        });

        //--------------------  Button action -------------------------
        btn_start_pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (btn_start_pause.getText().equals("start")) {
                    btn_start_pause.setText(getResources().getString(R.string.string_testMode_pause));
                    btn_start_pause.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.solid_conorradius50_red_ff3838));
                    btn_start_pause.setTextColor(Color.WHITE);

                    if (!tvTimer.getText().equals("00:00:00")) {
                        btn_resetDialog.setVisibility(View.INVISIBLE);
                        btn_upload.setVisibility(View.INVISIBLE);
                    }
                    // if running , editText can't edit
                    if (btn_start_pause.getText().equals("pause")) {
                        meditSpacing.setEnabled(false);
                    }
                    start();
                } else {
                    btn_start_pause.setText(getResources().getString(R.string.string_testMode_start));
                    btn_start_pause.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.border_radius50_green_56e251));
                    btn_start_pause.setTextColor(ContextCompat.getColor(getActivity(), R.color.green_56e251));

                    if (!tvTimer.getText().equals("00:00:00")) {
                        btn_resetDialog.setVisibility(View.VISIBLE);
                        btn_upload.setVisibility(View.VISIBLE);
                    }

                    // if no running editText can edit
                    if (btn_start_pause.getText().equals("start")) {
                        meditSpacing.setEnabled(true);
                    }

                    stop();
                }
            }
        });

        btn_resetDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetDialog();

            }
        });

        mLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Generalfunction().replaceFragment(getActivity(), new Fragment_main());
                if (objectMachineStatus != null) {
                    global.value_1st_sewing = objectMachineStatus.getLast_sewing();
                    global.value_1st_triming = objectMachineStatus.getLast_triming();
                }
            }
        });

        btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dataUpload();
            }
        });
        return view;
    }

    //  ---------------------------------- 計時器 function ------------------------------------------------------------
    private void start() {
        startTime = SystemClock.uptimeMillis();
        Timer();
        routineUpdateMachineStatus();

    }

    private void stop() {
        timeSwapBuff += timeInMilliseconds;
        TimerHandler.removeCallbacks(updateTimerThread);
        getDataHandler.removeCallbacks(sewingTriming);
    }

    private void Timer() {
        updateTimerThread = new Runnable() {
            public void run() {
                timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
                updateTime = timeSwapBuff + timeInMilliseconds;
                int ss = (int) (updateTime / 1000);
                int mm = ss / 60;
                ss %= 60;
                int ms = (int) (updateTime % 100);
                tvTimer.setText(formatTime(mm, ss, ms));
                TimerHandler.postDelayed(this, 0);
            }
        };

        TimerHandler.postDelayed(updateTimerThread, 0);
    }


    private String formatTime(int mm, int ss, int ms) {
        if (decimalFormat == null) {
            decimalFormat = new DecimalFormat("00");
        }
        min = decimalFormat.format(mm);
        sec = decimalFormat.format(ss);
        String mils = decimalFormat.format(ms);

        return min + ":" + sec + ":" + mils;
    }

    //------  Others  ----------------------
    private void resetDialog() {
        DialogFragment dialogFragment = new DF_pause();
        Bundle bundle = new Bundle();

        bundle.putInt("test_sec", Integer.parseInt(min) * 60 + Integer.parseInt(sec));
        bundle.putInt("sewing", Integer.parseInt(tv_sewing.getText().toString()));
        bundle.putInt("triming", Integer.parseInt(tv_triming.getText().toString()));
        bundle.putInt("length", Integer.parseInt(tv_length.getText().toString()));
        bundle.putString("test_time", formatter.format(currentTime) + "T" + formatter2.format(currentTime));
        bundle.putInt("spacing", Integer.parseInt(meditSpacing.getText().toString()));

        dialogFragment.setArguments(bundle);
        dialogFragment.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), "simple dialog");

    }

    private static void resetData() {
        tvTimer.setText("00:00:00");
        timeSwapBuff = 0;
        tv_length.setText("0");
        tv_sewing.setText("0");
        tv_triming.setText("0");
        value_sewing = 0;
        meditSpacing.setText("0");
        btn_resetDialog.setVisibility(View.INVISIBLE);
        btn_upload.setVisibility(View.INVISIBLE);
        global.value_1st_triming = value_2nd_triming;
        global.value_1st_sewing = value_2nd_sewing;
    }

    //------------------------------------------監聽 Edit ext-------------------------------------------------
    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        switch (actionId) {
            case EditorInfo.IME_NULL:
                System.out.println("Done_content: " + v.getText());
                break;
            case EditorInfo.IME_ACTION_SEND:
                System.out.println("send a email: " + v.getText());
                break;
            case EditorInfo.IME_ACTION_DONE:
                if (!meditSpacing.getText().toString().isEmpty()) {
                    tv_length.setText(String.valueOf(value_sewing * Integer.parseInt(meditSpacing.getText().toString())));

                } else {
                    meditSpacing.setText("0");
                    tv_length.setText("");
                }

                InputMethodManager imm = (InputMethodManager) Objects.requireNonNull(getActivity()).getSystemService(Context.INPUT_METHOD_SERVICE);
                assert imm != null;
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                meditSpacing.clearFocus();
                break;
        }

        return true;
    }

    //------------------------------------------BroadcastReceiver-----------------------------------------
    static class TestModeBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if ("reset".equals(intent.getAction())) {
                resetData();
            }
        }
    }

    // -------------------------- Update machine  info -----------------------------------
    private void routineUpdateMachineStatus() {
        sewingTriming = new Runnable() {
            public void run() {
                getDataHandler.postDelayed(this, 3000);
                Call<ApiMachineStatus> call = global.webapi.GetMachineStatus(global.macID);
                call.enqueue(new Callback<ApiMachineStatus>() {
                    @Override
                    public void onResponse(@NonNull Call<ApiMachineStatus> call,@NonNull  Response<ApiMachineStatus> response) {
                        objectMachineStatus = response.body();
                        assert objectMachineStatus != null;
                        if (objectMachineStatus.isSuccess()) {
                            value_2nd_sewing = objectMachineStatus.getLast_sewing();
                            value_2nd_triming = objectMachineStatus.getLast_triming();
                            int sewing_delta = value_2nd_sewing - global.value_1st_sewing;
                            int triming_delta = value_2nd_triming - global.value_1st_triming;
                            tv_sewing.setText(String.valueOf(sewing_delta));
                            tv_triming.setText(String.valueOf(triming_delta));
                            if (!meditSpacing.getText().toString().equals("")) {
                                tv_length.setText(String.valueOf(sewing_delta * Integer.parseInt(meditSpacing.getText().toString())));
                            }
                        } else {
                            Toast.makeText(getActivity(), objectMachineStatus.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ApiMachineStatus> call,@NonNull Throwable t) {
                        Toast.makeText(getActivity(), "網路連線異常.請檢察網路連線", Toast.LENGTH_LONG).show();
                    }
                });
            }
        };
        getDataHandler.post(sewingTriming);
    }

    //--------------------------------- Upload data ------------------------------------
    private void dataUpload() {

        final JsonObject paramObject = new JsonObject();
        paramObject.addProperty("mac", global.macID);
        paramObject.addProperty("model", global.objectMachineInfo.getModel());
        paramObject.addProperty("serial", global.objectMachineInfo.getSerial());
        paramObject.addProperty("test_sec", Integer.parseInt(min) * 60 + Integer.parseInt(sec));
        paramObject.addProperty("sewing", Integer.parseInt(tv_sewing.getText().toString()));
        paramObject.addProperty("triming", Integer.parseInt(tv_triming.getText().toString()));
        paramObject.addProperty("length", Integer.parseInt(tv_length.getText().toString()));
        paramObject.addProperty("test_time", formatter.format(currentTime) + "T" + formatter2.format(currentTime));
        paramObject.addProperty("spacing", Integer.parseInt(meditSpacing.getText().toString()));


        Call<ApiUploadtest> call = global.webapi.GetUploadTestResult(paramObject);
        call.enqueue(new Callback<ApiUploadtest>() {
            @Override
            public void onResponse(@NonNull Call<ApiUploadtest> call,@NonNull Response<ApiUploadtest> response) {
                objectApiUploadtest = response.body();
                assert objectApiUploadtest != null;
                if (objectApiUploadtest.isSuccess()) {

                    DialogFragment feedbackDialog = new DF_upload();
                    feedbackDialog.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), "tag1");
                } else {
                    DialogFragment feedbackDialog = new DF_upload();
                    Bundle bundle = new Bundle();
                    bundle.putString("errorInfo", objectApiUploadtest.getMessage().getErrorInfo().get(2));
                    feedbackDialog.setArguments(bundle);
                    feedbackDialog.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), "tag2");
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiUploadtest> call, @NonNull Throwable t) {
                Toast.makeText(getActivity(), "網路連線異常.請檢察網路連線", Toast.LENGTH_LONG).show();
            }
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        getDataHandler.removeCallbacks(sewingTriming);
        TimerHandler.removeCallbacks(updateTimerThread);
    }

    @Override
    public void onStop() {
        super.onStop();
        TimerHandler.removeCallbacks(updateTimerThread);
        getDataHandler.removeCallbacks(sewingTriming);
    }
}
