package tw.com.shoesconn.iot.webAPI;

public class ApiSetCountList {

    /**
     * success : true
     * message : null
     * time : 2020-06-08T13:11:19+08:00
     * ts : 1591593079
     */

    private boolean success;
    private Object message;
    private String time;
    private int ts;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getTs() {
        return ts;
    }

    public void setTs(int ts) {
        this.ts = ts;
    }
}
