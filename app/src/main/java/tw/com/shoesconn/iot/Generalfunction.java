package tw.com.shoesconn.iot;

import android.app.Activity;
import android.util.DisplayMetrics;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class Generalfunction {


    public void getWindowSize(Activity mActivity) {
        // Dialog 視窗大小
        DisplayMetrics dm = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        global.windowwidthPixels = dm.widthPixels;
        global.windowheightPixels = dm.heightPixels;
    }

    public void replaceFragment(Activity mActivity, Fragment fragment) {
        FragmentManager fragmentManager = ((AppCompatActivity) mActivity).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameContainer, fragment);
        fragmentTransaction.addToBackStack(fragment.toString());
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();

    }

}

