package tw.com.shoesconn.iot.view;

import android.app.Dialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;

import tw.com.shoesconn.iot.R;
import tw.com.shoesconn.iot.global;


public class DF_upload extends DialogFragment {

    private View view;
    private Button btn_ok;
    private Fragment_main_textmode.TestModeBroadcastReceiver mtestmodeBroadcaseReceiver = new Fragment_main_textmode.TestModeBroadcastReceiver();
    private Intent intent;
    private TextView title, message;


    public DF_upload() {
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout((int) (global.windowwidthPixels * 0.4), (int) (global.windowheightPixels * 0.4));
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.dialog_upload, container, false);
        btn_ok = view.findViewById(R.id.btn_upload);
        title = view.findViewById(R.id.string_dialog_title);
        message = view.findViewById(R.id.string_dialog_subtitle);

        Bundle bundle = getArguments();
        if (bundle != null) {
            title.setText("資料上傳失敗");
            message.setText(getArguments().getString("errorInfo"));
        } else {
            message.setVisibility(View.GONE);
        }


        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentFilter intentFilter = new IntentFilter("reset");
                getActivity().getApplicationContext().registerReceiver(mtestmodeBroadcaseReceiver, intentFilter);
                intent = new Intent();
                intent.setAction("reset");
                getActivity().getApplicationContext().sendBroadcast(intent);
                getDialog().cancel();
            }
        });

        return view;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (intent != null) {
            getActivity().getApplicationContext().unregisterReceiver(mtestmodeBroadcaseReceiver);
        }
    }

}
