package tw.com.shoesconn.iot.webAPI;

public class ApiLogin {


    /**
     * success : true
     * message : null
     * user_id : 1
     * worker_id : A0005
     * name : rock
     * occupation : 0
     * machine_mac : 00-11-22-33-44-55
     * token : eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC90YWlsZXJJT1QiLCJpYXQiOjE1ODg2NDQ4NzcsImV4cCI6MTU4ODcwMjQ3NywibWFjIjoiMDAtMTEtMjItMzMtNDQtNTUiLCJ1c2VyX2lkIjpudWxsfQ.u3eD5Mu8tj-qdSqBqW-xokserJkk5TClP_xh8W860XE
     */

    private boolean success;
    private int user_id;
    private String worker_id;
    private String name;
    private String occupation;
    private String machine_mac;
    private String token;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getWorker_id() {
        return worker_id;
    }

    public void setWorker_id(String worker_id) {
        this.worker_id = worker_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getMachine_mac() {
        return machine_mac;
    }

    public void setMachine_mac(String machine_mac) {
        this.machine_mac = machine_mac;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
