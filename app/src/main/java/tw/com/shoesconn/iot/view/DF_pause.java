package tw.com.shoesconn.iot.view;

import android.app.Dialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;

import tw.com.shoesconn.iot.R;
import tw.com.shoesconn.iot.global;
import tw.com.shoesconn.iot.webAPI.ApiUploadtest;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DF_pause extends DialogFragment {

    private View view;
    private Button btn_upload, btn_reset;
    private Fragment_main_textmode.TestModeBroadcastReceiver mtestmodeBroadcaseReceiver = new Fragment_main_textmode.TestModeBroadcastReceiver();
    private Intent intent;
    private ApiUploadtest objectApiUploadtest;

    public DF_pause() {
    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout((int) (global.windowwidthPixels * 0.4), (int) (global.windowheightPixels * 0.4));
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.dialog_pause, container, false);
        btn_upload = view.findViewById(R.id.btn_upload);
        btn_reset = view.findViewById(R.id.btn_reset);

        btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dataUpload();
            }
        });

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                IntentFilter intentFilter = new IntentFilter("reset");
                getActivity().getApplicationContext().registerReceiver(mtestmodeBroadcaseReceiver, intentFilter);
                intent = new Intent();
                intent.setAction("reset");
                getActivity().getApplicationContext().sendBroadcast(intent);
                getDialog().cancel();
            }
        });
        return view;
    }

    private void dataUpload() {

        JsonObject paramObject = new JsonObject();
        paramObject.addProperty("mac", global.macID);
        paramObject.addProperty("model",global.objectMachineInfo.getModel());
        paramObject.addProperty("serial", global.objectMachineInfo.getSerial());
        paramObject.addProperty("test_sec", getArguments().getInt("test_sec"));
        paramObject.addProperty("sewing", getArguments().getInt("sewing"));
        paramObject.addProperty("triming", getArguments().getInt("triming"));
        paramObject.addProperty("length", getArguments().getInt("length"));
        paramObject.addProperty("test_time", getArguments().getInt("test_time"));
        paramObject.addProperty("spacing", getArguments().getInt("spacing"));

        Call<ApiUploadtest> call = global.webapi.GetUploadTestResult(paramObject);
        call.enqueue(new Callback<ApiUploadtest>() {
            @Override
            public void onResponse(Call<ApiUploadtest> call, Response<ApiUploadtest> response) {
                objectApiUploadtest = response.body();
                if (objectApiUploadtest.isSuccess()) {
                    DialogFragment feedbackDialog = new DF_upload();
                    feedbackDialog.show(getActivity().getSupportFragmentManager(), "tag1");
                    getDialog().dismiss();
                } else {
                    DialogFragment feedbackDialog = new DF_upload();
                    Bundle bundle = new Bundle();
                    bundle.putString("errorInfo", objectApiUploadtest.getMessage().getErrorInfo().get(2));
                    feedbackDialog.setArguments(bundle);
                    feedbackDialog.show(getActivity().getSupportFragmentManager(), "tag2");
                    getDialog().dismiss();
                }
            }

            @Override
            public void onFailure(Call<ApiUploadtest> call, Throwable t) {
                Toast.makeText(getActivity(), "網路連線異常.請檢察網路連線", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (intent != null) {
            getActivity().getApplicationContext().unregisterReceiver(mtestmodeBroadcaseReceiver);
        }
    }

}
