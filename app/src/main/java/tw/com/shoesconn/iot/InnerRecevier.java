package tw.com.shoesconn.iot;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class InnerRecevier  extends BroadcastReceiver {

    final String SYSTEM_DIALOG_REASON_KEY = "reason";
    final String SYSTEM_DIALOG_REASON_RECENT_APPS = "recentApps";
    final String SYSTEM_DIALOG_REASON_HOME_KEY = "homeKey";


    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (Intent.ACTION_CLOSE_SYSTEM_DIALOGS.equals(action)){
            String reason = intent.getStringExtra(SYSTEM_DIALOG_REASON_KEY);
            if (reason != null){
                if (reason.equals(SYSTEM_DIALOG_REASON_HOME_KEY)){
                    Toast.makeText(context,"監聽HOME鍵",Toast.LENGTH_SHORT).show();
                }else if (reason.equals(SYSTEM_DIALOG_REASON_RECENT_APPS)){
                    Toast.makeText(context,"監聽多工鍵",Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


}
