package tw.com.shoesconn.iot.webAPI;

import java.util.List;

public class ApiSearchWorkSheet {


    /**
     * success : true
     * worksheet : [
     * {"name":"基本壓線指南",
     * "file_name":"cm_bqg01zht",
     * "file_type":"Document",
     * "file_time":"2020-03-25T04:00:09+08:00",
     * "url":"http://demoweb.gcreate.com.tw/tailorIOT/public/document/024b7a9f06dcc4e1b9ff9d6e11865a5d.pdf",
     * "cover_url":"http://demoweb.gcreate.com.tw/tailorIOT/public/cover/81906fe66930fc3e37753aaec77c7ca8.jpg"
     * }
     */

    private boolean success;
    private List<WorksheetBean> worksheet;
    private String message;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<WorksheetBean> getWorksheet() {
        return worksheet;
    }

    public void setWorksheet(List<WorksheetBean> worksheet) {
        this.worksheet = worksheet;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class WorksheetBean {
        /**
         * name : 基本壓線指南
         * file_name : cm_bqg01zht
         * file_type : Document
         * file_time : 2020-03-25T04:00:09+08:00
         * url : http://demoweb.gcreate.com.tw/tailorIOT/public/document/024b7a9f06dcc4e1b9ff9d6e11865a5d.pdf
         * cover_url : http://demoweb.gcreate.com.tw/tailorIOT/public/cover/81906fe66930fc3e37753aaec77c7ca8.jpg
         */

        private String name;
        private String file_name;
        private String file_type;
        private String file_time;
        private String url;
        private String cover_url;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getFile_name() {
            return file_name;
        }

        public void setFile_name(String file_name) {
            this.file_name = file_name;
        }

        public String getFile_type() {
            return file_type;
        }

        public void setFile_type(String file_type) {
            this.file_type = file_type;
        }

        public String getFile_time() {
            return file_time;
        }

        public void setFile_time(String file_time) {
            this.file_time = file_time;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getCover_url() {
            return cover_url;
        }

        public void setCover_url(String cover_url) {
            this.cover_url = cover_url;
        }
    }
}
