package tw.com.shoesconn.iot.view;

import android.os.Bundle;

import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import tw.com.shoesconn.iot.R;
import tw.com.shoesconn.iot.global;
import tw.com.shoesconn.iot.recyclerModel.Adapter_historicalData;
import tw.com.shoesconn.iot.webAPI.ApiMachineHistoryStatus;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.graphics.Color.WHITE;


public class Fragment_historical_data extends Fragment {

    private RecyclerView mRecyclerView;
    private FragmentActivity mContext;
    private ApiMachineHistoryStatus objMachineHistoryStatus;

    public Fragment_historical_data() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_historical_data, container, false);

        //---------- SearchView-----------
        SearchView mSearchview = view.findViewById(R.id.searchView);
        EditText searchEditText = mSearchview.findViewById(androidx.appcompat.R.id.search_src_text);
        ImageView searchIcon = mSearchview.findViewById(androidx.appcompat.R.id.search_button);
        ImageView searchcloseIcon = mSearchview.findViewById(androidx.appcompat.R.id.search_close_btn);

        searchEditText.setTextColor(WHITE);
        searchEditText.setHintTextColor(WHITE);
        searchIcon.setColorFilter(WHITE);
        searchcloseIcon.setColorFilter(WHITE);


        // --------- Recyclerview  --------------------
        mRecyclerView = view.findViewById(R.id.rv_historical_data);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        GetHistoryData();
        return view;
    }

    private void GetHistoryData(){
        Call<ApiMachineHistoryStatus> call = global.webapi.getMachineHistoryStatus(global.macID);
        call.enqueue(new Callback<ApiMachineHistoryStatus>() {
            @Override
            public void onResponse(Call<ApiMachineHistoryStatus> call, Response<ApiMachineHistoryStatus> response) {
                objMachineHistoryStatus = response.body();
                if (objMachineHistoryStatus.isSuccess() && objMachineHistoryStatus.getHistory() != null) {
                    mRecyclerView.setAdapter(new Adapter_historicalData(mContext, objMachineHistoryStatus.getHistory()));
                }else if (objMachineHistoryStatus.isSuccess() && objMachineHistoryStatus.getHistory() == null){
                    Toast.makeText(getActivity(), "無歷史紀錄", Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(getActivity(), objMachineHistoryStatus.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ApiMachineHistoryStatus> call, Throwable t) {
                Toast.makeText(getActivity(), "網路連線異常.請檢察網路連線", Toast.LENGTH_LONG).show();
            }
        });
    }
}
