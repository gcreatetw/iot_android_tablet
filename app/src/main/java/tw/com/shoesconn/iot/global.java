package tw.com.shoesconn.iot;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import tw.com.shoesconn.iot.view.Activity_login;
import tw.com.shoesconn.iot.webAPI.ApiLogin;
import tw.com.shoesconn.iot.webAPI.ApiMachieInfo;
import tw.com.shoesconn.iot.webAPI.Webapi;

import static tw.com.shoesconn.iot.webAPI.Webapi.SERVER_IP;

public class global {
    public static Context context;
    public static Webapi webapi;
    public static String LoginToken = "";
    public static String date = "";
    public static int windowwidthPixels, windowheightPixels;
    public static String macID;
    public static ApiMachieInfo objectMachineInfo;
    public static ApiLogin objectApiLogin;
    public static String YoutubeVideoID;
    public static int value_1st_sewing, value_1st_triming;
    public static boolean isappopen = false;


    public static void initWebAPI() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        if (global.LoginToken != "") {
                            Request.Builder builder = chain.request().newBuilder()
                                    .header("Bearer", global.LoginToken);
                            Request build = builder.build();
                            return chain.proceed(build);
                        } else {
                            Request.Builder builder = chain.request().newBuilder();
                            Request build = builder.build();
                            return chain.proceed(build);
                        }
                    }
                })
                .connectTimeout(3, TimeUnit.SECONDS)
                .readTimeout(3, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(SERVER_IP)
                .client(client) //Only for Web API debug
                .build();
        global.webapi = retrofit.create(Webapi.class);

    }

    public static void sendNotification(Context context, String messageTitle, String messageBody) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        String channelId = "default_notification_channel_id";
        String channelDescription = "Others";
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = notificationManager.getNotificationChannel(channelId);
            if (notificationChannel == null) {
                int importance = NotificationManager.IMPORTANCE_HIGH; //Set the importance level
                notificationChannel = new NotificationChannel(channelId, channelDescription, importance);
                notificationChannel.setLightColor(Color.GREEN); //Set if it is necesssary
                notificationChannel.enableVibration(true); //Set if it is necesssary
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }

        Intent intent = new Intent(context, Activity_login.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String[] split_line = messageBody.split("，");
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder;
        if (global.isappopen) {
            // in app catch notify
            notificationBuilder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.mipmap.iot_logo)
                    .setColor(context.getResources().getColor(R.color.colorPrimary))
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.iot_logo))
                    .setContentTitle(messageTitle)
                    .setContentText(messageBody)
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setChannelId(channelId);

        } else {
            notificationBuilder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.mipmap.iot_logo)
                    .setColor(context.getResources().getColor(R.color.colorPrimary))
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.iot_logo))
                    .setContentTitle(messageTitle)
                    .setContentText(messageBody)
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setChannelId(channelId)
                    .setContentIntent(pendingIntent);
        }


        if (split_line.length != 0) {
            notificationBuilder.setContentTitle(messageTitle);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

}
