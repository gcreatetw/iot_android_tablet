package tw.com.shoesconn.iot.webAPI;

public class ApiMachineStatus {


    /**
     * success : true
     * total_defective : 0
     * total_actual_output : 0
     * total_worktime : 0
     * total_targetl_output": 95753
     * last_triming: 0,
     * last_sewing: 0,
     * last_data_time" "2020-06-05 14:36:39"
     * message :
     */

    private boolean success;
    private String total_defective;
    private String total_actual_output;
    private String total_targetl_output;
    private String total_worktime;
    private String message;
    private int last_triming;
    private int last_sewing;
    private String last_data_time;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getTotal_defective() {
        return total_defective;
    }

    public void setTotal_defective(String total_defective) {
        this.total_defective = total_defective;
    }

    public String getTotal_actual_output() {
        return total_actual_output;
    }

    public void setTotal_actual_output(String total_actual_output) {
        this.total_actual_output = total_actual_output;
    }

    public String getTotal_worktime() {
        return total_worktime;
    }

    public void setTotal_worktime(String total_worktime) {
        this.total_worktime = total_worktime;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTotal_targetl_output() {
        return total_targetl_output;
    }

    public void setTotal_targetl_output(String total_targetl_output) {
        this.total_targetl_output = total_targetl_output;
    }

    public int getLast_triming() {
        return last_triming;
    }

    public void setLast_triming(int last_triming) {
        this.last_triming = last_triming;
    }

    public int getLast_sewing() {
        return last_sewing;
    }

    public void setLast_sewing(int last_sewing) {
        this.last_sewing = last_sewing;
    }

    public String getLast_data_time() {
        return last_data_time;
    }

    public void setLast_data_time(String last_data_time) {
        this.last_data_time = last_data_time;
    }
}
