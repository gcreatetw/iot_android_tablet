package tw.com.shoesconn.iot.webAPI;

import java.util.List;

public class ApiUploadtest {

    /**
     * success : false
     * message : {"errorInfo":["23000",1048,"Column 'test_sec' cannot be null"]}
     */

    private boolean success;
    private MessageBean message;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public MessageBean getMessage() {
        return message;
    }

    public void setMessage(MessageBean message) {
        this.message = message;
    }

    public static class MessageBean {
        private List<String> errorInfo;

        public List<String> getErrorInfo() {
            return errorInfo;
        }

        public void setErrorInfo(List<String> errorInfo) {
            this.errorInfo = errorInfo;
        }
    }
}
