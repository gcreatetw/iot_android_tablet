package tw.com.shoesconn.iot.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import tw.com.shoesconn.iot.R;
import tw.com.shoesconn.iot.global;
import tw.com.shoesconn.iot.recyclerModel.Adapter_searchworksheet;
import tw.com.shoesconn.iot.recyclerModel.Adapter_worksheet;
import tw.com.shoesconn.iot.webAPI.ApiSearchWorkSheet;
import tw.com.shoesconn.iot.webAPI.ApiWorkSheet;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.graphics.Color.WHITE;


public class Fragment_worksheet extends Fragment implements TextView.OnEditorActionListener {

    private View view;
    private Spinner mSpinner;
    private SearchView mSearchview;
    private EditText searchEditText;
    private ImageView searchIcon, searchcloseIcon;
    private RecyclerView mRecyclerView;
    private ApiWorkSheet objectApiWorkSheet;
    private ApiSearchWorkSheet objectApiSearchWorkSheet;
    private String selectedItemText;
    private static Adapter_worksheet adapter;
    private static Adapter_searchworksheet searchadapter;

    public Fragment_worksheet() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_worksheet, container, false);

        //--------     recyclerView -----------
        mRecyclerView = view.findViewById(R.id.rv_worksheet);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        //------------- Spinner -------------
        mSpinner = view.findViewById(R.id.worksheet_spinner);
        final ArrayList<String> spinnerCategory = new ArrayList<>();
        spinnerCategory.add("All");
        spinnerCategory.add("Document");
        spinnerCategory.add("Video");

        ArrayAdapter<String> arraydata = new ArrayAdapter<>(getActivity(), R.layout.spinner, spinnerCategory);
        mSpinner.setDropDownVerticalOffset(80);
        arraydata.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(arraydata);

        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedItemText = (String) adapterView.getItemAtPosition(i);
                if (global.objectMachineInfo.getModel() != null) {
                    if (searchEditText.getText().toString().isEmpty()) {
                        getWorkSheetData(selectedItemText);
                    } else {
                        getSearchWorkSheetData(selectedItemText);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //---------- SearchView-----------
        mSearchview = view.findViewById(R.id.searchView);
        mSearchview.clearFocus();
        searchEditText = mSearchview.findViewById(androidx.appcompat.R.id.search_src_text);
        searchIcon = mSearchview.findViewById(androidx.appcompat.R.id.search_button);
        searchcloseIcon = mSearchview.findViewById(androidx.appcompat.R.id.search_close_btn);

        searchEditText.setTextColor(WHITE);
        searchEditText.setHintTextColor(WHITE);
        searchIcon.setColorFilter(WHITE);
        searchcloseIcon.setColorFilter(WHITE);

        searchEditText.setOnEditorActionListener(this);
        mSearchview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchIcon.callOnClick();
            }
        });

        return view;
    }


    //------------------------ get machine info -----------------
    void getWorkSheetData(final String selectedItemText) {
        if (global.objectMachineInfo.getModel() != null) {
            Call<ApiWorkSheet> call = global.webapi.GetWorkSheetData(global.objectMachineInfo.getModel());
            call.enqueue(new Callback<ApiWorkSheet>() {
                @Override
                public void onResponse(Call<ApiWorkSheet> call, Response<ApiWorkSheet> response) {
                    objectApiWorkSheet = response.body();

                    final List<ApiWorkSheet.WorksheetBean> list = new ArrayList<>();

                    if (selectedItemText.equals("All")) {
                        adapter = new Adapter_worksheet(objectApiWorkSheet.getWorksheet(), selectedItemText);
                        mRecyclerView.setAdapter(adapter);
                        adapter.setOnItemClickListener(new Adapter_worksheet.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                if (objectApiWorkSheet.getWorksheet().get(position).getUrl().contains(".pdf")) {
                                    Intent intent = new Intent(getActivity(), Activity_pdfView.class);
                                    intent.putExtra("PDF_Url", objectApiWorkSheet.getWorksheet().get(position).getUrl());
                                    startActivity(intent);
                                } else {
                                    String[] videoID = objectApiWorkSheet.getWorksheet().get(position).getUrl().split("=");
                                    global.YoutubeVideoID = videoID[1];
                                    Intent intent = new Intent(getActivity(), Activity_youtubeView.class);
                                    intent.putExtra("lessonName", objectApiWorkSheet.getWorksheet().get(position).getName());
                                    intent.putExtra("videoDate",objectApiWorkSheet.getWorksheet().get(position).getFile_time());
                                    startActivity(intent);
                                }
                            }
                        });
                    } else if (selectedItemText.equals("Document")) {
                        for (int i = 0; i < objectApiWorkSheet.getWorksheet().size(); i++) {
                            if (objectApiWorkSheet.getWorksheet().get(i).getFile_type().equals("Document")) {
                                list.add(objectApiWorkSheet.getWorksheet().get(i));
                            }
                        }
                        adapter = new Adapter_worksheet(list, selectedItemText);
                        mRecyclerView.setAdapter(adapter);
                        adapter.setOnItemClickListener(new Adapter_worksheet.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                Intent intent = new Intent(getActivity(), Activity_pdfView.class);
                                intent.putExtra("PDF_Url", list.get(position).getUrl());
                                startActivity(intent);
                            }
                        });
                    } else if (selectedItemText.equals("Video")) {
                        for (int i = 0; i < objectApiWorkSheet.getWorksheet().size(); i++) {
                            if (objectApiWorkSheet.getWorksheet().get(i).getFile_type().equals("Video")) {
                                list.add(objectApiWorkSheet.getWorksheet().get(i));
                            }
                        }
                        adapter = new Adapter_worksheet(list, selectedItemText);
                        mRecyclerView.setAdapter(adapter);
                        adapter.setOnItemClickListener(new Adapter_worksheet.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                String[] videoID = list.get(position).getUrl().split("=");
                                global.YoutubeVideoID = videoID[1];
                                Intent intent = new Intent(getActivity(), Activity_youtubeView.class);
                                intent.putExtra("lessonName", list.get(position).getName());
                                intent.putExtra("videoDate",objectApiWorkSheet.getWorksheet().get(position).getFile_time());
                                startActivity(intent);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<ApiWorkSheet> call, Throwable t) {
                    Toast.makeText(getActivity(), "網路連線異常.請檢察網路連線", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        switch (i) {
            case EditorInfo.IME_ACTION_SEARCH:
                getSearchWorkSheetData(mSpinner.getSelectedItem().toString());
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                searchEditText.clearFocus();
                break;

        }
        return true;
    }


    //--------  GetSearchWorkSheetData --------------
    void getSearchWorkSheetData(final String selectedItemText) {

        JsonObject paramObject = new JsonObject();
        paramObject.addProperty("search", searchEditText.getText().toString());

        Call<ApiSearchWorkSheet> call = global.webapi.GetSearchWorkSheetResult(paramObject);
        call.enqueue(new Callback<ApiSearchWorkSheet>() {
            @Override
            public void onResponse(Call<ApiSearchWorkSheet> call, Response<ApiSearchWorkSheet> response) {

                objectApiSearchWorkSheet = response.body();
                final List<ApiSearchWorkSheet.WorksheetBean> list = new ArrayList<>();

                if (objectApiSearchWorkSheet.isSuccess() && !objectApiSearchWorkSheet.getWorksheet().isEmpty()) {

                    if (selectedItemText.equals("All")) {
                        searchadapter = new Adapter_searchworksheet(objectApiSearchWorkSheet.getWorksheet(), selectedItemText);
                        mRecyclerView.setAdapter(searchadapter);
                        searchadapter.setOnItemClickListener(new Adapter_searchworksheet.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                if (objectApiSearchWorkSheet.getWorksheet().get(position).getUrl().contains(".pdf")) {
                                    Intent intent = new Intent(getActivity(), Activity_pdfView.class);
                                    intent.putExtra("PDF_Url", objectApiSearchWorkSheet.getWorksheet().get(position).getUrl());
                                    startActivity(intent);
                                } else {
                                    String[] videoID = objectApiSearchWorkSheet.getWorksheet().get(position).getUrl().split("=");
                                    global.YoutubeVideoID = videoID[1];
                                    Intent intent = new Intent(getActivity(), Activity_youtubeView.class);
                                    intent.putExtra("lessonName", objectApiSearchWorkSheet.getWorksheet().get(position).getName());
                                    intent.putExtra("videoDate",objectApiSearchWorkSheet.getWorksheet().get(position).getFile_time());
                                    startActivity(intent);
                                }
                            }
                        });
                    } else if (selectedItemText.equals("Document")) {
                        for (int i = 0; i < objectApiSearchWorkSheet.getWorksheet().size(); i++) {
                            if (objectApiSearchWorkSheet.getWorksheet().get(i).getFile_type().equals("Document")) {
                                list.add(objectApiSearchWorkSheet.getWorksheet().get(i));
                            }
                        }
                        searchadapter = new Adapter_searchworksheet(list, selectedItemText);
                        mRecyclerView.setAdapter(searchadapter);
                        searchadapter.setOnItemClickListener(new Adapter_searchworksheet.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                Intent intent = new Intent(getActivity(), Activity_pdfView.class);
                                intent.putExtra("PDF_Url", list.get(position).getUrl());
                                startActivity(intent);
                            }
                        });
                    } else if (selectedItemText.equals("Video")) {
                        for (int i = 0; i < objectApiSearchWorkSheet.getWorksheet().size(); i++) {
                            if (objectApiSearchWorkSheet.getWorksheet().get(i).getFile_type().equals("Video")) {
                                list.add(objectApiSearchWorkSheet.getWorksheet().get(i));
                            }
                        }
                        searchadapter = new Adapter_searchworksheet(list, selectedItemText);
                        mRecyclerView.setAdapter(searchadapter);
                        searchadapter.setOnItemClickListener(new Adapter_searchworksheet.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                String[] videoID = list.get(position).getUrl().split("=");
                                global.YoutubeVideoID = videoID[1];
                                Intent intent = new Intent(getActivity(), Activity_youtubeView.class);
                                intent.putExtra("lessonName", list.get(position).getName());
                                intent.putExtra("videoDate",objectApiSearchWorkSheet.getWorksheet().get(position).getFile_time());
                                startActivity(intent);
                            }
                        });
                    }
                } else
                    Toast.makeText(getActivity(), "查無資料", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<ApiSearchWorkSheet> call, Throwable t) {
                Toast.makeText(getActivity(), "網路連線異常.請檢察網路連線", Toast.LENGTH_LONG).show();
            }
        });
    }
}
