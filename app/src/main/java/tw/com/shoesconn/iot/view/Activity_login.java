package tw.com.shoesconn.iot.view;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tw.com.shoesconn.iot.MainActivity;
import tw.com.shoesconn.iot.R;
import tw.com.shoesconn.iot.global;
import tw.com.shoesconn.iot.webAPI.ApiLogin;


public class Activity_login extends AppCompatActivity {

    private Button btn_signin;
    private EditText ed_employeeID, ed_employeePassword;
    private String token;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        global.initWebAPI();
        global.isappopen = true;

        ed_employeeID = findViewById(R.id.ed_employeeID);
        ed_employeePassword = findViewById(R.id.ed_employeePassword);

        String account = getSharedPreferences("data", MODE_PRIVATE).getString("account", "");
        ed_employeeID.setText(account);

        btn_signin = findViewById(R.id.btn_signIn);
        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!ed_employeeID.getText().toString().isEmpty() && !ed_employeePassword.getText().toString().isEmpty()) {
                    saveAccount();
                    GetLoginResult();
                } else {
                    Toast.makeText(Activity_login.this, "請輸入帳號或密碼", Toast.LENGTH_SHORT).show();
                }
            }
        });


        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }
                        if( task.getResult() == null)
                            return;
                        // Get new Instance ID token
                        token = task.getResult().getToken();
                        // Log and toast
                        Log.i("Activity","token : "+token);
                    }
                });
    }

    private void GetLoginResult() {

        JsonObject paramObject = new JsonObject();
        paramObject.addProperty("account", ed_employeeID.getText().toString());
        paramObject.addProperty("password", ed_employeePassword.getText().toString());
        paramObject.addProperty("deviceToken",token);

        Call<ApiLogin> call = global.webapi.GetLoginResult(paramObject);
        call.enqueue(new Callback<ApiLogin>() {
            @Override
            public void onResponse(Call<ApiLogin> call, Response<ApiLogin> response) {
                global.objectApiLogin = response.body();
                assert global.objectApiLogin != null;
                if (global.objectApiLogin.isSuccess()) {
                    if (global.objectApiLogin.getMachine_mac() != null) {
                        global.macID = global.objectApiLogin.getMachine_mac();
                        global.LoginToken = global.objectApiLogin.getToken();
                        Intent intent = new Intent(Activity_login.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                    } else
                        Toast.makeText(Activity_login.this, "此帳號無綁定 macID", Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(Activity_login.this, global.objectApiLogin.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<ApiLogin> call, Throwable t) {
                Toast.makeText(Activity_login.this, "網路連線異常.請檢察網路連線", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void saveAccount() {
        SharedPreferences.Editor editor = getSharedPreferences("data", MODE_PRIVATE).edit();
        editor.putString("account", ed_employeeID.getText().toString());
        editor.apply();
        //--  清空資料 ---
//                editor.clear().commit();
    }

}
