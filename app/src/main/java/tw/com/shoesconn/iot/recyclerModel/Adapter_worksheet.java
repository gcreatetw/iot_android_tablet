package tw.com.shoesconn.iot.recyclerModel;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import tw.com.shoesconn.iot.R;
import tw.com.shoesconn.iot.webAPI.ApiWorkSheet;

import java.util.List;


public class Adapter_worksheet extends RecyclerView.Adapter<Adapter_worksheet.ViewHolder> {


    private OnItemClickListener onItemClickListener = null;
    private List<ApiWorkSheet.WorksheetBean> worksheetBeans;
    private String spinnerSelectedText;

    public Adapter_worksheet(List<ApiWorkSheet.WorksheetBean> worksheetBeans, String spinnerSelectedText) {
        this.worksheetBeans = worksheetBeans;
        this.spinnerSelectedText = spinnerSelectedText;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        onItemClickListener = listener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_worksheet, parent, false);
        Adapter_worksheet.ViewHolder vh = new Adapter_worksheet.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        ApiWorkSheet.WorksheetBean worksheetBean = worksheetBeans.get(position);

        if (spinnerSelectedText.equals("Document") && worksheetBean.getFile_type().equals("Document")) {
            holder.card_tv_fileName.setText(worksheetBean.getFile_name());
            holder.card_tv_lesson_name.setText(worksheetBean.getName());
            Glide.with(holder.itemView).load(worksheetBean.getCover_url()).into(holder.card_video_img);
            holder.card_video_img.setClipToOutline(true);
            holder.card_tv_type.setText("Type : " + worksheetBean.getFile_type());
            holder.card_tv_date.setText("Date : " + worksheetBean.getFile_time().substring(0,10));
        }
        if (spinnerSelectedText.equals("Video") && worksheetBean.getFile_type().equals("Video")) {
            holder.card_tv_fileName.setText(worksheetBean.getFile_name());
            holder.card_tv_lesson_name.setText(worksheetBean.getName());
            Glide.with(holder.itemView).load(worksheetBean.getCover_url()).into(holder.card_video_img);
            holder.card_video_img.setClipToOutline(true);
            holder.card_tv_type.setText("Type : " + worksheetBean.getFile_type());
            holder.card_tv_date.setText("Date : " + worksheetBean.getFile_time().substring(0,10));
        }

        if (spinnerSelectedText.equals("All")) {
            holder.card_tv_fileName.setText(worksheetBean.getFile_name());
            holder.card_tv_lesson_name.setText(worksheetBean.getName());
            Glide.with(holder.itemView).load(worksheetBean.getCover_url()).into(holder.card_video_img);
            holder.card_video_img.setClipToOutline(true);
            holder.card_tv_type.setText("Type : " + worksheetBean.getFile_type());
            holder.card_tv_date.setText("Date : " + worksheetBean.getFile_time().substring(0,10));

        }
        //set action
        holder.btn_openVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(holder.btn_openVideo, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return worksheetBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        //init card
        private ImageView card_video_img;
        private TextView card_tv_fileName, card_tv_lesson_name, card_tv_type, card_tv_date;
        private Button btn_openVideo;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            this.card_video_img = itemView.findViewById(R.id.iv_video_img);
            this.card_tv_fileName = itemView.findViewById(R.id.tv_fileName);
            this.card_tv_lesson_name = itemView.findViewById(R.id.tv_lesson_name);
            this.card_tv_type = itemView.findViewById(R.id.tv_type);
            this.card_tv_date = itemView.findViewById(R.id.tv_date);
            this.btn_openVideo = itemView.findViewById(R.id.btn_openVideo);
        }
    }
}
