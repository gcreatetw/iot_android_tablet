package tw.com.shoesconn.iot.webAPI;

public class ApiCurrentTime {

    /**
     * success : true
     * current_time : 2020-05-04T14:32:30+08:00
     * ts : 1588573950
     */

    private boolean success;
    private String current_time;
    private int ts;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getCurrent_time() {
        return current_time;
    }

    public void setCurrent_time(String current_time) {
        this.current_time = current_time;
    }

    public int getTs() {
        return ts;
    }

    public void setTs(int ts) {
        this.ts = ts;
    }
}
